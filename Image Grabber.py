import sys
import requests
import json
import os
import bs4

user_agent = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36'
image_count = 0


def check(address):
    if type(address) == str:
        if any(ext in address for ext in ['.jpg','.jpeg','.tiff','.gif','.bmp','.png','.bat','.gifv','.webm','.mp4']):
            os.makedirs(os.path.join(path, folder_name), exist_ok = True)
            download_and_save(address)
            global image_count 
            image_count += 1
        elif address[:4] != 'http':
            print('Invalid address')
        else:
            response = requests.get(address, headers = {'User-agent': user_agent})
            try:
                response.raise_for_status()
                text = bs4.BeautifulSoup(response.text, "lxml")
                if 'google.' in address:
                    check(googleimages(address))
                elif 'imgur.com' in address:
                    check(imgur(address))
                elif 'reddit.com' in address:
                    check(reddit(text))
                else:
                    print('Site not yet supported:', address)
            except requests.exceptions.HTTPError:
                print('404 Client Error:', address, 'Not Found.')
    elif type(address) == list:
        for sub_address in address:
            check(sub_address)
    return None

def download_and_save(address):
    if any(ext in address for ext in ['.jpg?','.jpeg?','.tiff?','.gif?','.bmp?','.png?','.bat?','.gifv?','.webm?','.mp4?']):
        filename = address[:address.rfind('?')]
    else:
        filename = address
    print('Downloading image', os.path.basename(filename))
    image = requests.get(address, headers = {'User-agent': user_agent})
    image.raise_for_status()
    image_file = open(os.path.join(path, folder_name, os.path.basename(filename)), 'wb')
    for chunk in image.iter_content(100000):
        image_file.write(chunk)
    image_file.close()
    print('Saved!')
    return None


def googleimages(address):
    image_links = []
    new_link = 'https://www.google.com/search?q=' + address[address.find('=') +
        1:address.find('&')] + '&source=lnms&tbm=isch'

    try:
        response = requests.get(new_link, headers={'User-agent': user_agent})
        page = bs4.BeautifulSoup(response.text, "lxml")
        thumbs = page.find_all(attrs={'class':'rg_meta notranslate'})
        json_strs = [thumb.string for thumb in thumbs]
        for json_str in json_strs:
            j_obj = json.loads(json_str)
            image_links.append(j_obj['ou'])
    except requests.exceptions.HTTPError:
        print('404 Client Error:', address, 'Not Found.')

    return image_links


def imgur(address):
    image_links = []
    if not 'ajax' in address:
        id = address[-5:]
        address = 'http://imgur.com/ajaxalbums/getimages/' + id + '/hit.json?all=true'
    response = requests.get(address, headers={'User-agent': user_agent})
    try:
        response.raise_for_status()
        image_list = json.loads(response.text)
        for image in image_list['data']['images']:
            image_id = image['hash']
            image_ext = image['ext']
            image_links.append("https://i.imgur.com/" + image_id + image_ext)
    except requests.exceptions.HTTPError:
        print('404 Client Error:', address, 'Not Found.')
    return image_links


def reddit(text):
    posts = text.select('div[data-url]')
    image_links = []
    for index in range(len(posts)):
        image_links.append(posts[index].get('data-url'))
    print(len(image_links), 'posts found.')
    print('')
    return image_links


    if len(sys.argv) > 1:
        address = sys.argv[1]
    else:
        print("An error has occured")

    if len(sys.argv) > 2:
        path = sys.argv[2]
    else:
        path = ''

    if len(sys.argv) > 3:
        folder_name = sys.argv[3]
    else:
        folder_name = 'Pictures'

address = "https://seal.iacademy.edu.ph"
print('')
print('Downloading page:', address)
print('')
check(address)
print('')
print(image_count, 'images downloaded in', os.path.join(path, folder_name))
print('')
