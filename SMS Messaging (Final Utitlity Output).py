import requests
import json
import re

# THIS IS A PAID SEPHAMORE API-KEY. PLEASE DO NOT COPY OR USE.
# THIS PROGRAM IS FOR iACADEMY SEAL ONLY
api_key = "64fbd705fce8d36d759f12f55063c833"
account = requests.get("https://api.semaphore.co" +
                       "/api/v4/account?apikey="+api_key).json()
balance = int(account['credit_balance'])
go_ahead = True
ph_number = ""
number_regex = re.compile(r"^(09|\+639)\d{9}$")

while go_ahead:
    print("This system has " + str(balance) + " left.")
    while not number_regex.match(ph_number):
        ph_number = input("Please enter a valid" +
                          "Philippine Number (09xxxxxxxxxx: ")
    message = input("Enter your message here: ")
    r = requests.post("http://api.semaphore.co/api/v4/messages", data={
                                'number': ph_number,
                                'apikey': '64fbd705fce8d36d759f12f55063c833',
                                'message': message})
    data = r.json()
    status = data[0]
    print("====MESSAGE STATUS====")
    print("Message ID: " + str(status['message_id']))
    print("Account ID: " + str(status['account_id']))
    print("Account: " + str(status['account']))
    print("Recipient: " + str(status['recipient']))
    print("sender_name: " + str(status['sender_name']))
    print("Network: " + str(status['network']))
    print("Status: " + str(status['status']))
    balance = balance - 1
    ph_number = ""
    cont = input("Another one? (yes/no) > ")
    while cont.lower() not in ("yes", "no"):
        cont = input("Another one? (yes/no) > ")
        if cont == "no":
            go_ahead = False
